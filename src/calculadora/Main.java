/*
 * Calculadora hecha en java usando tecnologia AWT 
 * Swing y ActionListener
 */
package calculadora;

/**
 *
 * @author Jaasiel Guerra
 */
public class Main {

    
    public static void main(String[] args) {
        
        VistaPrincipal calculadora = new VistaPrincipal("Calculadora");
        calculadora.setSize(480,360);
        calculadora.setLocationRelativeTo(null);
        calculadora.setVisible(true);
    }
    
}
