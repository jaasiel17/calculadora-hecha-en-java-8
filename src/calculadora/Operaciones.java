/*
 * Clase encargada de las operaciones
 */
package calculadora;

/**
 *
 * @author Jaasiel Guerra
 */
public class Operaciones {    
    public  double operar(double a, double b, String operacion)//suma
    {
        switch(operacion)//evaluar operador para determinar la operacion
        {
            case "+": return a + b; //retorna suma
            case "-": return a - b; //retorna resta
            case "*": return a * b; //retorna multiplicacion
            case "/": return a / b; //retorna division
            case "pot": return Math.pow(a, b); //retorna potencia
            case "ra": return Math.pow(a,1/b); // retorna raiz, usando exponentes fracionarios
        }
        return 0;
    }    
}
