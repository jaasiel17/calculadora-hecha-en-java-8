/*
 * Esta clase hereda de JFrame y 
 * y es la Vista principal de la calculadora
 */
package calculadora;

import java.awt.BorderLayout;
import java.awt.HeadlessException;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JFrame;

/**
 *
 * @author Jaasiel Guerra
 */
public class VistaPrincipal extends JFrame{

    
    
    public VistaPrincipal(String title) throws HeadlessException {
        super(title);
        
        
        PanelCalculadora calculadora = new PanelCalculadora();//instancia del panel
        
        
        
       
        
        this.setLayout(new BorderLayout());
        
        this.add(calculadora,BorderLayout.CENTER);//agragar al centro
        
        
        
        
        this.addWindowListener(new WindowAdapter(){
            
            @Override
            public void windowClosing(WindowEvent ev)//evento cerrar ventana
            {
                System.exit(0);
                
            }
            
        });
        
    }
    
    
    
}
