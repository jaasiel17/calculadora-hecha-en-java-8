/*
 * Esta clase se encarga de crear los botones y la pantalla
 */
package calculadora;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

/**
 *
 * @author Jaasiel Guerra
 */
public class PanelCalculadora extends JPanel implements ActionListener {

    //atributos
    private JTextField display = null;
    private JButton BtnUno = null;
    private JButton BtnDos = null;
    private JButton BtnTres = null;
    private JButton BtnCuatro = null;
    private JButton BtnCinco = null;
    private JButton BtnSeis = null;
    private JButton BtnSiete = null;
    private JButton BtnOcho = null;
    private JButton BtnNueve = null;
    private JButton BtnCero = null;
    private JButton BtnSum = null;
    private JButton BtnRest = null;
    private JButton BtnMult = null;
    private JButton BtnDiv = null;
    private JButton BtnLimpiar = null;
    private JButton BtnIgual = null;
    private JPanel tecladoNum = null;
    private GridLayout layout = null;//layout para alinear los botones

    private String numUno = null, numDos = null, operacion = null;

    private int turno;

    private double resultado;

    private Operaciones operar = null;

    public PanelCalculadora() {
        initComponentes();
    }

    private void initComponentes() {

        display = new JTextField();
        BtnUno = new JButton();
        BtnDos = new JButton();
        BtnTres = new JButton();
        BtnCuatro = new JButton();
        BtnCinco = new JButton();
        BtnSeis = new JButton();
        BtnSiete = new JButton();
        BtnOcho = new JButton();
        BtnNueve = new JButton();
        BtnCero = new JButton();
        BtnSum = new JButton();
        BtnRest = new JButton();
        BtnMult = new JButton();
        BtnDiv = new JButton();
        BtnIgual = new JButton();
        BtnLimpiar = new JButton();
        tecladoNum = new JPanel();
        numUno = new String();
        numDos = new String();
        operacion = new String();
        turno = 1;
        resultado = 0;
        operar = new Operaciones();

        //activar la escucha de los eventos
        BtnUno.addActionListener(this);
        BtnDos.addActionListener(this);
        BtnTres.addActionListener(this);
        BtnCuatro.addActionListener(this);
        BtnCinco.addActionListener(this);
        BtnSeis.addActionListener(this);
        BtnSiete.addActionListener(this);
        BtnOcho.addActionListener(this);
        BtnNueve.addActionListener(this);
        BtnCero.addActionListener(this);
        BtnSum.addActionListener(this);
        BtnRest.addActionListener(this);
        BtnMult.addActionListener(this);
        BtnDiv.addActionListener(this);
        BtnIgual.addActionListener(this);
        BtnLimpiar.addActionListener(this);

        layout = new GridLayout(4, 3);//4 filas y 4 columnas

        layout.setHgap(10);//separacion Horizontal del layout
        layout.setVgap(10);//separacion vertical del layout

        display.setPreferredSize(new Dimension(WIDTH, 50));//tamanio del campo de texto
        display.setFont(new Font("ocr a std", 1, 30));//estilo de letra del display
        display.setHorizontalAlignment(SwingConstants.RIGHT);
        display.setBackground(Color.WHITE);
        display.setEditable(false);
        display.setText("0");
        
        BtnUno.setFont(new Font("ocr a std", 1, 25));
        BtnDos.setFont(new Font("ocr a std", 1, 25));
        BtnTres.setFont(new Font("ocr a std", 1, 25));
        BtnCuatro.setFont(new Font("ocr a std", 1, 25));
        BtnCinco.setFont(new Font("ocr a std", 1, 25));
        BtnSeis.setFont(new Font("ocr a std", 1, 25));
        BtnSiete.setFont(new Font("ocr a std", 1, 25));
        BtnOcho.setFont(new Font("ocr a std", 1, 25));
        BtnNueve.setFont(new Font("ocr a std", 1, 25));
        BtnCero.setFont(new Font("ocr a std", 1, 25));
        BtnSum.setFont(new Font("ocr a std", 1, 25));
        BtnRest.setFont(new Font("ocr a std", 1, 25));
        BtnMult.setFont(new Font("ocr a std", 1, 25));
        BtnDiv.setFont(new Font("ocr a std", 1, 25));
        BtnLimpiar.setFont(new Font("ocr a std", 1, 25));
        BtnIgual.setFont(new Font("ocr a std", 1, 25));
        
        
        

        BtnUno.setText("1");
        BtnDos.setText("2");
        BtnTres.setText("3");
        BtnCuatro.setText("4");
        BtnCinco.setText("5");
        BtnSeis.setText("6");
        BtnSiete.setText("7");
        BtnOcho.setText("8");
        BtnNueve.setText("9");
        BtnCero.setText("0");
        BtnSum.setText("+");
        BtnRest.setText("-");
        BtnMult.setText("*");
        BtnDiv.setText("/");
        BtnIgual.setText("=");
        BtnLimpiar.setText("C");

        tecladoNum.setLayout(layout);

        tecladoNum.add(BtnSiete);
        tecladoNum.add(BtnOcho);
        tecladoNum.add(BtnNueve);
        tecladoNum.add(BtnSum);
        tecladoNum.add(BtnCuatro);
        tecladoNum.add(BtnCinco);
        tecladoNum.add(BtnSeis);
        tecladoNum.add(BtnRest);
        tecladoNum.add(BtnUno);
        tecladoNum.add(BtnDos);
        tecladoNum.add(BtnTres);
        tecladoNum.add(BtnMult);
        tecladoNum.add(BtnCero);
        tecladoNum.add(BtnLimpiar);
        tecladoNum.add(BtnIgual);
        tecladoNum.add(BtnDiv);

        this.setLayout(new BorderLayout());//setear layout
        this.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));//agregar margen al panel

        this.add(display, BorderLayout.NORTH);//agregando el display
        this.add(tecladoNum, BorderLayout.CENTER);//agregando el tecladoNum
    }

    
        private void turnos(JButton num) {//para asignar valores segun turno

        if (turno == 1)//si turno uno, es primer operando
        {
            numUno += num.getText();//asigna texto a primer cadena
            display.setText(numUno);

        } else if (turno == 2) {//si es segundo turno

            numDos += num.getText(); //asigna texto a segunda cadena
            display.setText(numUno +  operacion + numDos);
        }
    }

    private void operador(JButton operador) {//para asignar los operadores
        
        if (turno == 1 && turno != 0) {

            operacion = operador.getText();
            turno = 2;

            display.setText(numUno  + operacion + numDos);
        }
    }

    
    @Override
    public void actionPerformed(ActionEvent e) {

        if (BtnUno == e.getSource()) {

            turnos(BtnUno);

        }

        if (BtnDos == e.getSource()) {

            turnos(BtnDos);
        }
        
        if (BtnTres == e.getSource()) {

            turnos(BtnTres);
        }
        
        if (BtnCuatro == e.getSource()) {

            turnos(BtnCuatro);
        }
        
        if (BtnCinco == e.getSource()) {

            turnos(BtnCinco);
        }
        
        if (BtnSeis == e.getSource()) {

            turnos(BtnSeis);
        }
        
        if (BtnSeis == e.getSource()) {

            turnos(BtnSeis);
        }
        
        if (BtnSiete == e.getSource()) {

            turnos(BtnSiete);
        }
        
        if (BtnOcho == e.getSource()) {

            turnos(BtnOcho);
        }
        
        if (BtnNueve == e.getSource()) {

            turnos(BtnNueve);
        }
        
        if (BtnCero == e.getSource()) {
            
            turnos(BtnCero);
            
        }

        if (BtnSum == e.getSource()) {
            
            operador(BtnSum);
            
        }
        
        if (BtnRest == e.getSource()) {
            
            operador(BtnRest);
            
        }
        
        if (BtnMult == e.getSource()) {
            
            operador(BtnMult);
            
        }
        
        if (BtnDiv == e.getSource()) {
            
            operador(BtnDiv);
            
        }

        if (BtnIgual == e.getSource()) {

            if (turno == 2) {

                resultado = operar.operar(Double.parseDouble(numUno),
                        Double.parseDouble(numDos), operacion);

                display.setText(numUno  + operacion  + numDos + "=" + resultado);

                turno = 0;

            }

        }

        if (BtnLimpiar == e.getSource()) {

            display.setText("0");
            numUno = "";
            numDos = "";
            resultado = 0;
            turno = 1;

        }

    }

}
